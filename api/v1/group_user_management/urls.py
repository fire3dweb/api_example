from django.conf.urls import url
from api.v1.group_user_management.views.group_user_view import GroupUserView, GroupDetail

urlpatterns = [
    url(r'^$', GroupUserView.as_view()),
    url(r'^(?P<pk>[\w\.-]+)/$', GroupDetail.as_view()),
]
