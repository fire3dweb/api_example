from api.v1.group_user_management.serializers.group_user_serializer import GroupUserSerializer
from apps.user_management.models import GroupUser
from django.http import Http404


def get_groups():
    groups = GroupUser.objects.all()
    serializer = GroupUserSerializer(groups, many=True)
    return serializer.data


def get_group_by_id(id):
    try:
        group = GroupUser.objects.get(pk=id)
        serializer = GroupUserSerializer(group, many=False)
        return serializer.data
    except GroupUser.DoesNotExist:
        raise Http404
    except Exception as e:
        raise


def save_group(serializer):
    try:
        serializer.save()

    except Exception as e:
        raise
