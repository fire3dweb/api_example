from rest_framework import serializers
from apps.user_management.models import GroupUser


class GroupUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupUser
        fields = ('id', 'name', 'description')
        read_only = ('id',)

