from api.v1.group_user_management.managers.group_user_manager import get_groups, get_group_by_id, save_group
from api.v1.group_user_management.serializers.group_user_serializer import GroupUserSerializer


class GroupUserController():
    @staticmethod
    def get_group(id):
        return get_group_by_id(id)

    @staticmethod
    def get_groups():
        return get_groups()

    @staticmethod
    def save_group(data):
        serializer = GroupUserSerializer(data=data)
        if serializer.is_valid():
            save_group(serializer)
            return serializer
        return serializer
