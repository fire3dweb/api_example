from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from api.v1.group_user_management.controllers.group_user_controller import GroupUserController


class GroupUserView(APIView):
    permission_classes = ()

    def post(self, request, format=None):
        try:
            user = GroupUserController.save_group(request.data)
            if hasattr(user, 'errors') and user.errors:
                return Response(user.errors, status=status.HTTP_400_BAD_REQUEST)

            return Response(user.data, status=status.HTTP_201_CREATED)

        except Exception as e:
            return Response(data={'message': "Unexpected error occurred : {}".format(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request):
        try:
            data = GroupUserController.get_groups()

            return Response(data)
        except Exception as e:
            return Response({'message': "Unexpected error occurred.....{}".format(e)}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class GroupDetail(APIView):
    """

    """
    permission_classes = ()

    def get(self, request, pk):
        user = GroupUserController.get_group(pk)
        return Response(user)
