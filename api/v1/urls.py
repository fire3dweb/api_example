from django.conf.urls import url, include

urlpatterns = [
    url(r'^group/', include('api.v1.group_user_management.urls', namespace='group_user')),
    url(r'^user/', include('api.v1.user_management.urls', namespace='user')),
]
