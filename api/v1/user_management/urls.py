from django.conf.urls import url
from api.v1.user_management.views.user_view import UserView, UserDetail

urlpatterns = [
    url(r'^$', UserView.as_view()),
    url(r'^(?P<pk>[\w\.-]+)/$', UserDetail.as_view()),
]
