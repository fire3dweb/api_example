from api.v1.user_management.managers.user_manager import get_users, get_user_by_id, save_user
from api.v1.user_management.serializers.user_serializer import UserSerializer


class UserController():
    @staticmethod
    def get_user(id):
        return get_user_by_id(id)

    @staticmethod
    def get_users():
        return get_users()

    @staticmethod
    def save_user(data):
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            save_user(serializer)
            return serializer
        return serializer
