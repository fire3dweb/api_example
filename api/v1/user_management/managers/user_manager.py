from api.v1.user_management.serializers.user_serializer import UserSerializer
from apps.user_management.models import UserTest
from django.http import Http404


def get_users():
    """
    Esta funcion retorna todos los usuarios de la tabla user_test
    :return: Object type users
    """
    users = UserTest.objects.all()
    serializer = UserSerializer(users, many=True)
    return serializer.data


def get_user_by_id(id):
    try:
        user = UserTest.objects.get(pk=id)
        serializer = UserSerializer(user, many=False)
        return serializer.data
    except UserTest.DoesNotExist:
        raise Http404
    except Exception as e:
        raise


def save_user(serializer):
    try:
        serializer.save()

    except Exception as e:
        raise
