from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from api.v1.user_management.controllers.user_controller import UserController


class UserView(APIView):
    permission_classes = ()

    def post(self, request, format=None):
        try:
            user = UserController.save_user(request.data)
            if hasattr(user, 'errors') and user.errors:
                return Response(user.errors, status=status.HTTP_400_BAD_REQUEST)

            return Response(user.data, status=status.HTTP_201_CREATED)

        except Exception as e:
            return Response(data={'message': "Unexpected error occurred : {}".format(e)},
                            status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def get(self, request):
        try:
            data = UserController().get_users()

            return Response(data)
        except Exception:
            return Response({'message': "Unexpected error occurred"}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UserDetail(APIView):
    """

    """
    permission_classes = ()

    def get(self, request, pk):
        user = UserController.get_user(pk)
        return Response(user)
