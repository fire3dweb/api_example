from rest_framework import serializers
from apps.user_management.models import UserTest, GroupUser


class UserSerializer(serializers.ModelSerializer):
    group = serializers.PrimaryKeyRelatedField(queryset=GroupUser.objects.all())

    class Meta:
        model = UserTest
        fields = ('id', 'name', 'password', 'age', 'group')
        read_only = ('id',)

        extra_kwargs = {
            'password': {'write_only': True},
        }
