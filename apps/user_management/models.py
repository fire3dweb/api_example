from django.db import models


class GroupUser(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)

    class Meta:
        db_table = "group_user"


class UserTest(models.Model):
    name = models.CharField(max_length=255, unique=True)
    age = models.IntegerField(blank=True, null=True)
    password = models.CharField(max_length=255)
    group = models.ManyToManyField(GroupUser)

    class Meta:
        db_table = "user_test"
