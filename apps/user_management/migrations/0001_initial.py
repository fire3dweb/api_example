# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-09 18:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GroupUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=255)),
            ],
            options={
                'db_table': 'group_user',
            },
        ),
        migrations.CreateModel(
            name='UserTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
                ('age', models.IntegerField(blank=True, null=True)),
                ('password', models.CharField(max_length=255)),
                ('group', models.ManyToManyField(to='user_management.GroupUser')),
            ],
            options={
                'db_table': 'user_test',
            },
        ),
    ]
