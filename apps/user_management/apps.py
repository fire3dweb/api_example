from django.apps import AppConfig


class UserManagementConfig(AppConfig):
    name = 'apps.user_management'
